import spotifyUri, {ParsedSpotifyUri, Playlist, Track} from 'spotify-uri';
import base64 from 'base-64';
import fetch from 'node-fetch';

export default class SpotifyApi {

    private readonly _clientId: string;
    private readonly _clientSecret: string;
    private _token: string;
    private _expiresAt: number;

    constructor(clientId: string, clientSecret: string) {
        this._clientId = clientId;
        this._clientSecret = clientSecret;
    }

    async getTrack(url: string): Promise<SpotifyTrack | undefined> {
        const parsed: ParsedSpotifyUri = spotifyUri.parse(url);
        if (parsed instanceof Track) {
            const result = await this.getData(`https://api.spotify.com/v1/tracks/${parsed.id}`);
            if(!result.error) {
                return result;
            } else {
                return undefined;
            }
        } else {
            return undefined;
        }
    }

    async getPlaylistTracks(url): Promise<Array<SpotifyTrack>> {
        try {
            const parsed: ParsedSpotifyUri = spotifyUri.parse(url);
            if (parsed instanceof Playlist) {
                const tracks: Array<SpotifyTrack> = [];
                let countWhile = 0;
                do {
                    const result: Array<SpotifyTrack> = await this.getData(`https://api.spotify.com/v1/playlists/${parsed.id}/tracks?offset=${countWhile * 100}&limit=100`);
                    tracks.push(...(result['items'].map(entry => entry['track'])))
                    countWhile++;
                } while (tracks.length === countWhile * 100);
                return tracks;
            } else {
                return undefined
            }
        } catch {
            return undefined;
        }
    }



    private async getToken() {
        const body = new URLSearchParams();
        body.append('grant_type', 'client_credentials');
        const response = await fetch('https://accounts.spotify.com/api/token', {
            method: 'POST',
            headers: {
                'Authorization': 'Basic ' + base64.encode(this._clientId + ':' + this._clientSecret)
            },
            body
        })
        const result = await response.json();
        this._expiresAt = Date.now() + result['expires_in'];
        this._token = result['access_token'];
    }

    private isLoggedIn() {
        return this._token !== undefined && this._expiresAt > Date.now() + 2;
    }

    private async login() {
        if (!this.isLoggedIn()) {
            await this.getToken();
        }
    }

    private async getData(url: string) {
        await this.login();
        const response = await fetch(url, {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + this._token
            }
        })
        return await response.json();
    }


}

export type SpotifyPlaylist = {
    collaborative: boolean
    description: string
    external_urls: ExternalUrl
    followers: Followers
    href: string
    id: string
    images: Array<ImageObject>
    name: string
    owner: PublicUser
    public: boolean
    snapshot_id: string
    tracks: Array<PlaylistTrack>
    type: string
    uri: string
}

export type PublicUser = {
    display_name: string
    external_urls: ExternalUrl
    followers: Followers
    href: string
    id: string
    images: Array<ImageObject>
    type: string
    uri: string
}

export type PlaylistTrack = {
    added_at: number
    added_by: PublicUser
    is_local: boolean
    track: SpotifyTrack | Episode
}

export type Episode = {
    audio_preview_url: string
    description: string
    duration_ms: number
    explicit: boolean
    external_urls: ExternalUrl
    href: string
    html_description: string
    id: string
    images: Array<ImageObject>
    is_externally_hosted: boolean
    is_playable: boolean
    language: string
    languages: Array<string>
    name: string
    release_date: string
    release_date_precision: string
    restrictions: EpisodeRestriction
    resume_point: ResumePoint
    show: SimplifiedShow
    type: string
    uri: string
}

export type Copyright = {
    text: string
    type: string
}

export type SimplifiedShow = {
    available_markets: Array<string>
    copyrights: Array<Copyright>
    description: string
    explicit: boolean
    external_urls: ExternalUrl
    href: string
    html_description: string
    id: string
    images: Array<ImageObject>
    is_externally_hosted: boolean
    languages: Array<string>
    media_type: string
    name: string
    publisher: string
    type: string
    uri: string
}

export type ResumePoint = {
    fully_played: boolean
    resume_position_ms: number
}

export type Artist = {
    external_urls: ExternalUrl
    followers: Followers
    genres: Array<string>
    href: string
    id: string
    images: Array<ImageObject>
    name: string
    popularity: number
    type: string
    uri: string
}

export type Followers = {
    href: string
    total: number
}

export type ImageObject = {
    height: number
    url: string
    width: number
}

export type SpotifyTrack = {
    album: SimplifiedAlbum
    artists: Array<Artist>
    available_markets: Array<string>
    disc_number: number
    duration_ms: number
    explicit: boolean
    external_ids: ExternalId
    external_urls: ExternalUrl
    href: string
    id: string
    is_local: boolean
    is_playable: boolean
    name: string
    popularity: number
    preview_url: string | null
    restrictions?: TrackRestriction
    track_number: number
    type: string
    uri: string
}

export type ExternalId = {
    ean: string
    isrc: string
    upc: string
}

export type SimplifiedAlbum = {
    album_group: string
    album_type: string
    artists: Array<SimplifiedArtist>
    available_markets: Array<string>
    external_urls: ExternalUrl
    href: string
    id: string
    images: Array<ImageObject>
    name: string
    release_date: string
    release_date_precision: string
    restrictions: AlbumRestriction
    total_tracks: number
    type: string
    uri: string
}

export type SimplifiedArtist = {
    external_urls: ExternalUrl
    href: string
    id: string
    name: string
    type: string
    uri: string
}

export type ExternalUrl = {
    spotify: string
}

export type Restriction = {
    reason: 'market' | 'product' | 'explicit' | string;
}

export type AlbumRestriction = Restriction

export type TrackRestriction = Restriction

export type EpisodeRestriction = Restriction