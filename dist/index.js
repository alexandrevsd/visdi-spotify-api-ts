"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const spotify_uri_1 = __importStar(require("spotify-uri"));
const base_64_1 = __importDefault(require("base-64"));
const node_fetch_1 = __importDefault(require("node-fetch"));
class SpotifyApi {
    constructor(clientId, clientSecret) {
        this._clientId = clientId;
        this._clientSecret = clientSecret;
    }
    getTrack(url) {
        return __awaiter(this, void 0, void 0, function* () {
            const parsed = spotify_uri_1.default.parse(url);
            if (parsed instanceof spotify_uri_1.Track) {
                const result = yield this.getData(`https://api.spotify.com/v1/tracks/${parsed.id}`);
                if (!result.error) {
                    return result;
                }
                else {
                    return undefined;
                }
            }
            else {
                return undefined;
            }
        });
    }
    getPlaylistTracks(url) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const parsed = spotify_uri_1.default.parse(url);
                if (parsed instanceof spotify_uri_1.Playlist) {
                    const tracks = [];
                    let countWhile = 0;
                    do {
                        const result = yield this.getData(`https://api.spotify.com/v1/playlists/${parsed.id}/tracks?offset=${countWhile * 100}&limit=100`);
                        tracks.push(...(result['items'].map(entry => entry['track'])));
                        countWhile++;
                    } while (tracks.length === countWhile * 100);
                    return tracks;
                }
                else {
                    return undefined;
                }
            }
            catch (_a) {
                return undefined;
            }
        });
    }
    getToken() {
        return __awaiter(this, void 0, void 0, function* () {
            const body = new URLSearchParams();
            body.append('grant_type', 'client_credentials');
            const response = yield node_fetch_1.default('https://accounts.spotify.com/api/token', {
                method: 'POST',
                headers: {
                    'Authorization': 'Basic ' + base_64_1.default.encode(this._clientId + ':' + this._clientSecret)
                },
                body
            });
            const result = yield response.json();
            this._expiresAt = Date.now() + result['expires_in'];
            this._token = result['access_token'];
        });
    }
    isLoggedIn() {
        return this._token !== undefined && this._expiresAt > Date.now() + 2;
    }
    login() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.isLoggedIn()) {
                yield this.getToken();
            }
        });
    }
    getData(url) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.login();
            const response = yield node_fetch_1.default(url, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this._token
                }
            });
            return yield response.json();
        });
    }
}
exports.default = SpotifyApi;
//# sourceMappingURL=index.js.map